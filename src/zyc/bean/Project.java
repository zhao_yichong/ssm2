package zyc.bean;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/12/1 22:41
 * @version: v
 * @modified By:
 */
public class Project {
    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", poll=" + poll +
                ", user=" + user +
                '}';
    }

    private Integer id;
    private String content;
    private Integer poll=0;
    private User user;

    public Project() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Project(Integer id, String content, Integer poll, User user) {
        this.id = id;
        this.content = content;
        this.poll = poll;
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getPoll() {

        return poll;
    }

    public void setPoll(Integer poll) {
        this.poll = poll;
    }



}
