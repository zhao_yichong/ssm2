package zyc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zyc.bean.Project;
import zyc.dao.FprojectMapper;

import java.util.List;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/12/1 22:48
 * @version: v1.0
 * @modified By:
 */
@Service
public class FprojectService {
    @Autowired
    private FprojectMapper fprojectMapper;
    public List<Project > getFprojects(Project  project) {
        return fprojectMapper.getFprojects(project);
    }
}
