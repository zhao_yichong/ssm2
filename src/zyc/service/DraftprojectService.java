package zyc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zyc.bean.Project;
import zyc.dao.DraftprojectMapper;

import java.util.List;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/12/1 22:48
 * @version: v1.0
 * @modified By:
 */
@Service
public class DraftprojectService {
    @Autowired
    private DraftprojectMapper draftprojectMapper;
    public List<Project> getProjects(Project project) {
        return draftprojectMapper.getProjects(project);
    }
}
