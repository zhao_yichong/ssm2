package zyc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zyc.bean.Project;
import zyc.dao.T_projectMapper;

import java.util.List;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/12/1 22:48
 * @version: v1.0
 * @modified By:
 */
@Service
public class T_projectService {

    @Autowired
    private  T_projectMapper t_projectMapper;
    public List<Project > getProjects(Project  project) {
        return t_projectMapper.getProjects(project);
    }

    public Project  getproject(Project  project) {
        return t_projectMapper.getproject(project );
    }

    public void creatProject(Project project) {
        project.setId(null);
        t_projectMapper.creatProject(project);
    }

    public void updateConten(Project project) {
        t_projectMapper.updateConten(project);
    }
}
