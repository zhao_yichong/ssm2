package zyc.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import zyc.bean.User;
import zyc.service.UserService;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/11/29 11:16
 * @version: v1.0
 * @modified By:
 */
@Controller
public class RootController {

}
