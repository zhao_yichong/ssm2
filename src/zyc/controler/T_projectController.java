package zyc.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import zyc.bean.Project;
import zyc.bean.User;
import zyc.service.ProjectService;
import zyc.service.T_projectService;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/11/29 11:16
 * @version: v1.0
 * @modified By:
 */
@Controller
public class T_projectController {

    @Autowired
    private T_projectService t_projectService;
    @Autowired
    private ProjectService projectService;

    // 接收 从通过审核传过来修改的项目
    @RequestMapping(value = "/project",method = RequestMethod.PUT)
    public  String updateContent(Project project,Map<String,Object> map){
        projectService.deletProjectByid(project.getId());
        //creat 会把id 设为null
        t_projectService.creatProject(project);

        return "forward:/getTprojects/"+project.getUser().getId();
    }
    //插入新项目
    @RequestMapping(value = "/project", method = RequestMethod.POST)
    public String inserNewProject(@RequestParam("u_id") Integer uid, Project project, Map<String, Object> map) {


        project.setUser(new User(uid, null, null, null));
        t_projectService.creatProject(project);
        return "forward:/getTprojects/" + uid;
    }

    //获取 在审核的项目
    @RequestMapping("/getTprojects/{u_id}")
    public String getTproject(@PathVariable("u_id")Integer uid, Map<String,Object> map){
        List<Project > projects = t_projectService.getProjects(new Project (null, null, null, new User(uid)));
        map.put("projects",projects);
        return "show/tproject";

    }
}
