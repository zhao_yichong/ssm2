package zyc.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import zyc.bean.Project;
import zyc.bean.User;
import zyc.service.DraftprojectService;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/11/29 11:16
 * @version: v1.0
 * @modified By:
 */
@Controller
public class DraftprojectController {
    @Autowired
    private DraftprojectService draftprojectService;

    @RequestMapping("/getDraftprojects/{u_id}")
    public String getDraftprojects(@PathVariable("u_id")Integer uid,Map<String,Object> map){
        List<Project> projects = draftprojectService.getProjects(new Project(null, null, null, new User(uid)));
        map.put("projects",projects);
        return "show/draftproject";
    }

}
