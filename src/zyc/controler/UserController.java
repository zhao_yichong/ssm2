package zyc.controler;


import javax.mail.Session;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import zyc.bean.Project;
import zyc.bean.User;
import zyc.service.ProjectService;
import zyc.service.UserService;

import javax.jws.soap.SOAPBinding;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TransferQueue;

import static com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/11/29 11:16
 * @version: v1.0
 * @modified By:
 */
@Controller
@SessionAttributes(value = {"user"} )
public class UserController {
    @Autowired
    private UserService userService;


    //修改用户的target 转到project 修改poll
    @RequestMapping("/setTarget")
    public  String setTarget(@RequestParam("uid") Integer uid,@RequestParam("pid") Integer pid,@RequestParam(value = "poll",required = false) Integer poll,Map<String,Object> map){

        User user = userService.selectUser(new User(uid));



        if(user.getTarget()==0 || user.getTarget()==null){
            user.setTarget(pid);
        }else{
            user.setTarget(0);
        }

        try {
            userService.update(user);

            map.put("user",userService.selectUser(new User(uid)));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        if(pid==0){
            return "forward:/getprojects/" +uid;
        }else{
            return "forward:/setPoll?id="+pid+"&poll="+poll;
        }



    }

    //用户修改信息之前经过的路径
    @RequestMapping(value = "/upuser",method = RequestMethod.GET)
    public String getupuser(  Map<String,Object> map,HttpServletRequest req){
        User user = (User) req.getSession().getAttribute("user");
        map.put("user",user);

        return "upandcreat/updateuser";

    }
    //修改信息的主要逻辑
    @RequestMapping(value = "/upuser",method = RequestMethod.POST)
    public String upuser( User user,Map<String,Object> map,HttpServletRequest req,@RequestParam("code") String code){

        String toke = (String) req.getSession().getAttribute(KAPTCHA_SESSION_KEY);

        req.getSession().removeAttribute(KAPTCHA_SESSION_KEY);


        if(!toke.equals(code)){
            map.put("alert","验证码错误");

            map.put("user",userService.selectUser(new User(user.getId())));

            return "upandcreat/updateuser";
        }
        try {
            userService.update(user);
            map.put("alert","信息修改成功");
            map.put("user",user);
        }catch (Exception e){
            map.put("alert","你修改的名字已经有人注册");
            map.put("user",userService.selectUser(new User(user.getId())));
            return "upandcreat/updateuser";
        }


        return "upandcreat/updateuser";

    }

    //用户注销 返回到登录页面
    @RequestMapping("/logout")
    public String logout(Map<String,Object> map){

        map.put("user",new User());

        return "user/login";
    }

    //用户点击登录 后到这里 获取个 user域 方便form表单
    @RequestMapping(value = "/loginuser" ,method = RequestMethod.GET)
    public String getNewUserL(Map<String,Object> map){


        map.put("user",new User());

        return "user/login";
    }

    //用户登录的页面 转发到 projectController  获取项目

    @RequestMapping(value = "/loginuser",method = RequestMethod.POST)
    public String loginuser( User user, BindingResult bindingResult,
                             Map<String, Object> map){


        if(bindingResult.getErrorCount()>0){
            return "user/login";
        }else {

            User user1= userService.selectUser(user);

            if(user1!=null){
                map.put("user",user1);


                return "redirect:/getprojects/"+user1.getId();
            }else {
                map.put("alert","用户名或密码错误");
                return "user/login";
            }
        }
    }




    //用户点击注册  后到这里 获取个 user域 方便form表单
    @RequestMapping(value = "/registuser" ,method = RequestMethod.GET)
    public String getNewUserR(Map<String,Object> map){

        map.put("user",new User());
        return "user/registuser";
    }


    //用户注册的主要逻辑
    @RequestMapping(value = "/registuser" ,method = RequestMethod.POST)
    public String registuser(@Valid User user,BindingResult bindingResult, Map<String,Object> map,HttpServletRequest req,@RequestParam("code") String code){

        String toke = (String) req.getSession().getAttribute(KAPTCHA_SESSION_KEY);

        req.getSession().removeAttribute(KAPTCHA_SESSION_KEY);

        if(!toke.equals(code)){
            map.put("alert","验证码错误！");
            return "user/registuser";
        }
        if(bindingResult.getErrorCount()>0){
            return "user/registuser";
        }
        try {

            userService.registuser(user);
        }catch (Exception e){
            System.out.println(e.getMessage());
            map.put("alert","用户名重复");
            return "user/registuser";
        }finally {

        }
        map.put("user",user);

        return "redirect:/getprojects/"+user.getId();
    }

    @RequestMapping("/getUsers")
    public String getUsers(Map<String,Object> map){
        List<User> users = userService.getUsers();
        map.put("users",users);
        return "forward:/index.jsp";

    }


}
