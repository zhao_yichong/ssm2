package zyc.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import zyc.bean.Project;
import zyc.bean.User;
import zyc.service.FprojectService;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/11/29 11:16
 * @version: v1.0
 * @modified By:
 */
@Controller
public class FprojectController {

    @Autowired
    private FprojectService fprojectService;
    //
    @RequestMapping("/getFprojects/{u_id}")
    public String getFprojects(@PathVariable("u_id")Integer id, Map<String,Object> map){
        List<Project > projects = fprojectService.getFprojects(new Project (null, null, null, new User(id)));
        map.put("projects",projects);
        return "show/fproject";
    }
}
