package zyc.dao;


import org.springframework.stereotype.Repository;
import zyc.bean.User;

import java.util.List;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/11/20 17:56
 * @version: v1.0
 * @modified By:
 */
@Repository
public interface UserMapper {


    public List<User> selectUsers();
    public User selectUserByid(Integer id);
    public User selectUser(User user);
    void inseruser(User user);

    void updateuser(User user);
}
