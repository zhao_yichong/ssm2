<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>投票系统登录页面</title>
	<%@include file="/pages/common/head.jsp"%>

	<script type="text/javascript">
		$(function () {

			<c:if test="${not empty requestScope.alert}">
			alert("${requestScope.alert}")

			</c:if>
		});

	</script>
</head>
<body>
		<div id="login_header">
			<img class="logo_img" alt="" src="static/img/logo.gif" >
		</div>
		
			<div class="login_banner">




				<div class="content">
					<div class="login_form">
						<div class="login_box">
							<div class="tit">
								<h1>用户登录</h1>
								<a href="/registuser">立即注册</a>
							</div>


								<form:form action="/loginuser" modelAttribute="user" method="post">

									name:<form:input path="name"></form:input>
									<form:errors path="name"></form:errors>
									<br>
									password:<form:password path="password"></form:password>
									<form:errors path="password"></form:errors>
									<br>
									<input type="submit" value="Submit">


								</form:form>


							</div>
							
						</div>
					</div>
				</div>
			</div>
		<%@include file="/pages/common/foote.jsp"%>
</body>
</html>