<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>项目管理页面</title>
    <%@include file="/pages/common/head.jsp"%>
    <style type="text/css">
        h1 {
            text-align: center;
            margin-top: 200px;
        }

        h1 a {
            color:red;
        }

        input {
            text-align: center;
        }
    </style>

    <script type="text/javascript">
        $(function () {


            $("#creat").click(function () {

                if( !confirm('你确定要创建吗？')){
                    return false;
                }
            })
            $("#update").click(function () {

                if( !confirm('你确定要修改吗？')){
                    return false;
                }
            })
        });

    </script>
</head>
<body>
<div id="header">
    <img class="logo_img" alt="" src="../../static/img/logo.gif" >
    <span class="wel_word">项目创建修改处</span>
    <%@include file="/pages/common/manager_menu.jsp"%>
</div>

<

<div id="main">

    <c:if test="${ not empty requestScope.project.id}">
        <form:form action="/project" method="post" modelAttribute="project">
            <input type="hidden" name="_method" value="PUT">
            <table>
                <tr>
                    <td>项目内容</td>
                    <td colspan="2">操作</td>
                </tr>
                <td>
                    <td><form:input path="content"></form:input></td>
                    <td><form:hidden path="id"></form:hidden></td>
                <td><form:hidden path="user.id"></form:hidden></td>
                <td><form:hidden path="poll"></form:hidden></td>





                    <td><input type="submit" id="update" value="修改"/></td>
                </tr>
            </table>
        </form:form>
    </c:if>



    <c:if test="${ empty requestScope.project.id}">
        <form:form action="/project" method="post" modelAttribute="project">
            <input type="hidden" name="u_id" value="${sessionScope.user.id}">
            <table>
                <tr>
                    <td>项目内容</td>
                    <td colspan="2">操作</td>
                </tr>
                <tr>
                    <td><form:input path="content"></form:input></td>

                    <td><input  id="creat" type="submit" value="创建"/></td>
                </tr>
            </table>
        </form:form>
    </c:if>







</div>

<%@include file="/pages/common/foote.jsp"%>
</body>
</html>