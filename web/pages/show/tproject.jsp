<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>尚硅谷会员注册页面</title>
	<%@include file="/pages/common/head.jsp"%>
	<style type="text/css">
		h1 {
			text-align: center;
			margin-top: 200px;
		}

		h1 a {
			color:red;
		}
	</style>

	<script type="text/javascript" src="scripts/jquery-1.9.1.min.js"></script>
<%--	<script type="text/javascript">--%>
<%--		$(function () {--%>
<%--			$(".delete").click(function () {--%>
<%--				var href=$(this).attr("href");--%>
<%--				$("form").attr("action",href).submit();--%>
<%--				return false;--%>
<%--			});--%>
<%--		})--%>
<%--	</script>--%>

	<script type="text/javascript">

		$(function () {

			<c:if test="${not empty requestScope.alert}">

			alert("${requestScope.alert}")
			</c:if>

			$("a.delete").click(function () {

				if( !confirm("你确定要删除你的项目："+$(this).parent().parent().find("td:first").text()+"？")){
					return false;
				}else{
					var href=$(this).attr("href");
					$("form").attr("action",href).submit();
					return false;
				}
			});
			$("a.end").click(function () {


				if( !confirm("你确定要结束投票吗？")){
					return false;
				}
			});
			$("a.creatP").click(function () {


				if( !confirm("你确定要创建项目吗？")){
					return false;
				}
			});
			$("a.creatJ").click(function () {


				if( !confirm("你确定要创建评委吗？")){
					return false;
				}
			});
			$("a.start").click(function () {


				if( !confirm("你确定要开启投票吗？")){
					return false;
				}
			});
		});

	</script>
</head>
<body>
<div id="header">
	<img class="logo_img" alt="" src="static/img/logo.gif" >
	<%@include file="/pages/common/loginsucces_menu.jsp"%>
</div>
<form action="" method="post">
	<input type="hidden" name="_method" value="DELETE">
</form>

<div id="main">

	<table>







		<c:if test="${not empty requestScope.projects}">

			<tr>
				<td>项目展示</td>
				<td>项目票数</td>
				<td></td>

				<td colspan="2">操作</td>
			</tr>
			<c:forEach items="${requestScope.projects}" var="project">
				<tr>
					<td>${project.content}</td>
					<td>${project.poll}</td>
					<td></td>
					<td><a class="delete" href="/project/${project.id}">取消审核</a></td>
				</tr>
			</c:forEach>

		</c:if>

</table>

</div>


<%@include file="/pages/common/foote.jsp"%>
</body>
</html>