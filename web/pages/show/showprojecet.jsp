<%@ page import="zyc.bean.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>投票地点</title>
	<%@include file="/pages/common/head.jsp"%>

	<script type="text/javascript">



		$(function () {

			$("a.vote").click(function () {


				if( !confirm("你确定给"+$(this).parent().parent().find("td:first").text()+"投票吗？")){
					return false;
				}
			});

			$("a.canclevote").click(function () {


				if( !confirm("你确定取消 对"+$(this).parent().parent().find("td:first").text()+"的投票吗？")){
					return false;
				}
			});
		});


	</script>

</head>
<body>

<c:if test="${empty sessionScope.user}">

	<%

		request.getRequestDispatcher("/loginuser").forward(request,response);

	%>

</c:if>


	
	<div id="header">
			<img class="logo_img" alt="" src="../../static/img/logo.gif" >

		<%@include file="/pages/common/manager_menu.jsp"%>
	</div>
	
	<div id="main">
		<span style="margin-left: 40%;" >${requestScope.head}</span>
		<table>
			<tr>
				<td>项目负责人</td>
				<td>项目展示</td>
				<td>目前票数</td>

				<td colspan="2">操作</td>
			</tr>

			<c:if test="${  sessionScope.user.target==0}">



				<c:if test="${empty requestScope.projects}">
					<%
						pageContext.forward("/getProjectsToTou");

					%>
				</c:if>

				<c:if test="${not empty requestScope.projects }">
					<c:forEach items="${requestScope.projects}" var="project">



						<tr>
							<td>${project.user.name}</td>
							<td>${project.content}</td>
							<td>${project.poll}</td>

								<%--							<c:if test="${user.endflag==1}">--%>
								<%--								<td>该项目投票已结束</td>--%>
								<%--							</c:if>--%>
								<%--							<c:if test="${user.endflag==0}">--%>

							<td><a  class="vote" href="/setTarget?uid=${sessionScope.user.id}&pid=${project.id}&poll=${project.poll+1}">投ta</a></td>
								<%--							</c:if>--%>

						</tr>


					</c:forEach>
				</c:if>

			</c:if>

			<c:if test="${ sessionScope.user.target!=0}">
				<c:if test="${empty requestScope.project}">
					<%

						User user = (User) request.getSession().getAttribute("user");
						pageContext.forward("/getProjectToTou/"+user.getTarget());

						%>
				</c:if>

			<c:if test="${not empty requestScope.project}">
				<tr>
					<td>${requestScope.project.user.name}</td>
					<td>${requestScope.project.content}</td>
					<td>${requestScope.project.poll}</td>




					<td><a href="index.jsp">去查看结果</a></td>
					<td><a  class="canclevote" href="/setTarget?uid=${sessionScope.user.id}&pid=${project.id}&poll=${project.poll-1}">取消投票</a></td>



				</tr>
			</c:if>



			</c:if>






			

			
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>

			</tr>
		</table>
	</div>

	<%@include file="/pages/common/foote.jsp"%>
</body>
</html>